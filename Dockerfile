FROM node
MAINTAINER Alexandr Fedotov <spavin4@gmail.com>
RUN mkdir -p /opt/frontend
ADD ./ /opt/frontend
WORKDIR /opt/frontend
RUN npm install && npm run build && npm install -g serve
CMD serve -s build -l 8080