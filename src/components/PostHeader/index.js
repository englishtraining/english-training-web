import React, {Component} from "react";
import {commentAddRequestAction, commentListGetByIdRequestAction} from "../../reducers/actions/post-actions";
import {connect} from "react-redux";
import {Breadcrumb, Button, Comment, Container, Form, Grid, Header} from "semantic-ui-react";
import {Link} from "react-router-dom";

class PostHeader extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    mapLinks(links) {
        return links
            .map((link, a) => {
                if (a < links.length - 1) {
                    return [
                        <Breadcrumb.Section link>
                            <Link to = { link.url }>{ link.name }</Link>
                        </Breadcrumb.Section>,
                        <Breadcrumb.Divider icon='right angle'/>
                    ]
                } else {
                    return <Breadcrumb.Section link>
                                <Link to = { link.url }>{ link.name }</Link>
                            </Breadcrumb.Section>;
                }
            });
    }

    render() {
        const { name, links } = this.props;
        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Section link>
                        <Link to="/">Главная</Link>
                    </Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right angle'/>
                    { this.mapLinks(links) }
                </Breadcrumb>
                <Header size='huge'>{ name }</Header>
            </div>
        );
    }
}

export default PostHeader;