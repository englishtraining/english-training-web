import React, {Component} from "react";
import {commentAddRequestAction, commentListGetByIdRequestAction} from "../../reducers/actions/post-actions";
import {connect} from "react-redux";
import {Button, Container, Modal, Header, Segment, Tab} from "semantic-ui-react";
import CommentList from '../CommentList';

class ShowComment extends Component {

    styles = {
        showCommentStyle: {
            height: '700px',
            width: '100%',
            bottom: '0',
            display: 'none'
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            commentListValue: [],
            showedSelectedCommentListValue: false
        };

        this.selectedCommentListValue = 0;
    }

    componentDidMount() {
    }

    componentWillUpdate() {
    }

    componentWillReceiveProps(props) {
        const { commentList } = this.props;

        if (commentList.length !== props.commentList.length) {
            this.setState({ commentListValue: this.mapCommentList(props.items, props.commentList, props.getCommentList)});
        } else {
            for (var a = 0; a < commentList.length; a++) {
                if (commentList[a].id !== props.commentList[a].id) {
                    this.setState({ commentListValue: this.mapCommentList(props.items, props.commentList, props.getCommentList)});

                    break;
                }
            }
        }
    }

    getCommentList(event, data) {
        const { getCommentList } = this.props;

        this.selectedCommentListValue = data.activeIndex;

        getCommentList(data.panes[data.activeIndex].id);

        this.showSelectedCommentListValue();
    }

    addCommentById(chunkIds, comment) {
        const { postId, addComment } = this.props;

        let data = { postId, chunkIds, comment };

        addComment(data);

        this.setState({ showedAddComment: false });
    }

    mapCommentList(items, commentList, getCommentList) {
        let commentIdList = commentList
            .map(item => {
                return { chunkIds: item.chunkIds, id: item.id }
            });
        let commentValueList = commentIdList
            .map(item => item.chunkIds
                .map(id => items[id].text)
                .reduce((a, b) => a + ' ' + b, ''));
        let commentData = [];

        for (let a = 0; a < commentIdList.length; a++) {
            commentData.push({ ...commentIdList[a], value: commentValueList[a] });
        }

        if (commentIdList.length > 0) {
            getCommentList(commentIdList[0].id);
        }

        return commentData
            .map(comment => {
                return { id: comment.id, menuItem: comment.value, commentList:
                    <CommentList chunkIds = { comment.chunkIds } addComment = {(chunkIds, comment) => this.addCommentById(chunkIds, comment)}/> }
            });
    }

    _showComment() {
        const { showComment } = this.props;

        this.selectedCommentListValue = 0;

        showComment();
    }

    showSelectedCommentListValue() {
        const { showedSelectedCommentListValue } = this.state;

        this.setState({ showedSelectedCommentListValue: !showedSelectedCommentListValue });
    }

    render() {
        const { commentListValue, showedSelectedCommentListValue } = this.state;
        const { showedComment } = this.props;
        const { showCommentStyle } = this.styles;

        return <Segment className = 'show-comment' style = { showedComment ? { ...showCommentStyle, display: 'block' } : showCommentStyle }>
                    <Container textAlign = 'right'>
                        <Button onClick = { () => this.showSelectedCommentListValue() } circular icon='list ul' color = 'blue'
                                style = { { margin: '0 0 5px 0' } }/>
                        <Button onClick = { () => this._showComment() } circular icon='close' color = 'red'
                                style = { { margin: '0 0 5px 0' } }/>
                    </Container>
                    <Header as='h2'>
                        { commentListValue.length > 0 ? commentListValue[this.selectedCommentListValue].menuItem : null }
                    </Header>
                    { commentListValue.length > 0 ? commentListValue[this.selectedCommentListValue].commentList : null }
                    <Modal
                        open={ showedSelectedCommentListValue }
                        size='small'
                                onClose={ () => this.showSelectedCommentListValue() }
                                style = {{ top: '100px' }}
                        closeIcon>
                        <Modal.Content>
                            <Tab activeIndex = {this.selectedCommentListValue } menu={{ color: 'blue', inverted: true, fluid: true, vertical: true }} panes={ commentListValue } onTabChange = {(event, data) => this.getCommentList(event, data)} />
                        </Modal.Content>
                    </Modal>
                </Segment>;
    }
}

function mapStateToProps(state) {
    return {  };
}

function mapDispatchToProps(dispatch) {
    return {
        getCommentList: id => dispatch(commentListGetByIdRequestAction(id)),
        addComment: comment => dispatch(commentAddRequestAction(comment))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowComment);