import React, {Component} from "react";
import {Image, Item} from "semantic-ui-react";

class Author extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { authorName, authorPhoto, data } = this.props;

        return (
            <Item>
                <Image src={ authorPhoto } circular width='100px' height='100px'/>
                <Item.Content>
                    <Item.Header as='a'>{ authorName }</Item.Header>
                    <Item.Meta>
                    </Item.Meta>
                    <Item.Meta>
                        Дата публикации: { data }
                    </Item.Meta>
                </Item.Content>
            </Item>
        );
    }
}

export default Author;