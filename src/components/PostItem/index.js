import React, {Component} from 'react';
import './PostItem.css';

class PostItem extends Component {

    styles = {
        postItem: {
            display: 'inline-block'
        }
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    render() {
        const { item } = this.props;
        const { styles } = this;

        return (
            <div style={ styles.postItem }>
               { item.text }
            </div>
        );
    }
}

export default PostItem