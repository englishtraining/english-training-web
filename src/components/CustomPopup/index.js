import React, { Component } from 'react';
import { connect } from "react-redux";
import { Segment } from 'semantic-ui-react'
import './Popup.css'

class CustomPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            show: undefined
        };
        
    };

    componentWillReceiveProps(nextProps) {

        if (nextProps.message !== undefined) {
            this.setState({
                message: nextProps.message
            });            
        };
        
        this.setState({
            show: nextProps.show
        });
    }

    render() {
        const style = { background: this.props.success ? '' : '#FFA79E' };
        let setClass = 'custom-popup animated ';
        if (this.state.show !== undefined) {
            setClass += (this.state.show ? ' fadeInRight ' : ' fadeOutRight');
        }
               
        return (
            <div className={setClass}
                style={style}>
                {this.state.message}
            </div>
        );
    };
}

function mapStateToProps(state) {
    return { ...state.popup };
}

export default connect(mapStateToProps)(CustomPopup);