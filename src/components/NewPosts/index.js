import React, {Component} from "react";
import { postListRequestAction } from "../../reducers/actions/post-actions";
import {connect} from "react-redux";
import {Card, Feed, Item, Label, Rating} from "semantic-ui-react";
import {formatDate} from "../../service/UtilService";
import {Link} from "react-router-dom";

class NewPosts extends Component {

    constructor(props) {
        super(props);

        this.postList = [];
    }

    componentDidMount() {
        this.props.getPostList();
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    componentWillReceiveProps(props) {
        const { postList } = props;
        let propsPostList = postList.slice(postList.length - 5, postList.length);

        for (let a = 0; a < postList.length; a++) {
            if (postList[a].postId !== propsPostList[a].postId) {
                this.postList = propsPostList;

                this.setState({});

                break;
            }
        }
    }

    mapPostList(posts) {
        return posts
            .map(item => <Item>
                            <Item.Image size='tiny' src = { item.authorPhoto } />
                            <Item.Content>
                                <Item.Header as='a'>
                                    <Link to={`/post/${item.postId}`}>{item.title}</Link>
                                </Item.Header>
                                <Item.Meta>{ formatDate(new Date(item.date)) }</Item.Meta>
                                <Item.Description>
                                    <Label size= 'mini'>Tag</Label>
                                    <Label size= 'mini'>Tag</Label>
                                    <Label size= 'mini'>Tag</Label>
                                    <Label size= 'mini'>Tag</Label>
                                </Item.Description>
                                <Item.Extra>
                                    <Rating icon='star' defaultRating={ 4 } maxRating={ 5 }/>
                                </Item.Extra>
                            </Item.Content>
                        </Item>);
    }

    render() {
        const { postList } = this;

        return <Card style = {{ width: '100%' }}>
                    <Card.Content>
                        <Card.Header>Новые публикации</Card.Header>
                    </Card.Content>
                    <Card.Content>
                        <Item.Group>
                            { this.mapPostList(postList) }
                        </Item.Group>
                    </Card.Content>
                </Card>;
    }
}

function mapStateToProps(state) {
    return { postList: state.posts.postList ? state.posts.postList : [] };
}

function mapDispatchToProps(dispatch) {
    return {
        getPostList: () => dispatch(postListRequestAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPosts);