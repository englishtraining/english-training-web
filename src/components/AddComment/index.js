import React, {Component} from "react";
import {commentAddRequestAction, commentListGetByIdRequestAction} from "../../reducers/actions/post-actions";
import {connect} from "react-redux";
import {Button, Comment, Container, Form, Header, Modal, Segment, Tab} from "semantic-ui-react";

class AddComment extends Component {

    styles = {
        addCommentStyle: {
            height: '400px',
            width: '100%',
            bottom: '0',
            display: 'none'
        }
    };

    constructor(props) {
        super(props);

        this.comment = '';
    }

    setComment(event, data) {
        this.comment = data.value;
    }

    addComment() {
        const { comment } = this;
        const { selectedItems } = this.props;

        let chunkIds = selectedItems
            .map(item => item.chunkId)
            .filter(item => item);

        this.addCommentById(chunkIds, comment);
    }

    addCommentById(chunkIds, comment) {
        const { postId, addComment, showAddComment } = this.props;

        let data = { postId, chunkIds, comment };

        addComment(data);

        showAddComment();
    }

    mapSelectedItemValue() {
        return this.props.selectedItems
            .map(item => item.text)
            .reduce((a, b) => a + ' ' + b, '');
    }

    render() {
        const { showedAddComment, showAddComment } = this.props;

        let value = this.mapSelectedItemValue();

        return <Modal
                    open={ showedAddComment }
                    size='small'
                    onClose={ () => showAddComment() }
                    style = {{ top: '100px' }}
                    closeIcon>
                    <Modal.Content>
                        <Header as='h2'>
                            Добавьте комментрий к: <br/>
                            { value }
                        </Header>
                        <Form>
                            <Form.TextArea style = { { height: '200px' } } onChange={ (event, data) => this.setComment(event, data) }/>
                            <Button onClick={ () => this.addComment() } content='Добавить комментарий' icon='edit' color = 'blue'/>
                        </Form>
                    </Modal.Content>
                </Modal>;
    }
}

function mapStateToProps(state) {
    return {  };
}

function mapDispatchToProps(dispatch) {
    return {
        addComment: comment => dispatch(commentAddRequestAction(comment))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddComment);