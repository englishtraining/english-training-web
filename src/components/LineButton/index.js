import React, {Component} from "react";
import {commentAddRequestAction, commentListGetByIdRequestAction} from "../../reducers/actions/post-actions";
import {connect} from "react-redux";
import {Button, Comment, Container, Form} from "semantic-ui-react";
import TransitionablePortalEnglish from "../TransitionablePortal/TransitionablePortal";

class LineButton extends Component {

    styles = {
        lineButtonDialog: {
            position: 'absolute',
            transform: 'translateX(-50%)',
            width: '60px'
        },
        lineButton: {
            background: 'white',
            width: '100%',
            height: '100%',
            padding: '10px 10px 10px 10px',
            border: '1px black solid',
            borderRadius: "10px 10px 10px 10px"
        }
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    render() {
        const { showedLineButton, lineButtonA, lineButtonB, showAddComment } = this.props;
        const { lineButtonDialog, lineButton } = this.styles;

        return (
            <TransitionablePortalEnglish open={ showedLineButton }>
                <Container style = { { ...lineButtonDialog, left: lineButtonA, top: lineButtonB } }>
                    <Container style = { lineButton }>
                        <Button onClick={ showAddComment } icon='edit' circular primary/>
                    </Container>
                </Container>
            </TransitionablePortalEnglish>
        );
    }
}

export default LineButton;