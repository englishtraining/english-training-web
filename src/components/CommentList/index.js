import React, {Component} from "react";
import {commentAddRequestAction, commentListGetByIdRequestAction} from "../../reducers/actions/post-actions";
import {connect} from "react-redux";
import {Button, Comment, Container, Form} from "semantic-ui-react";

class CommentItem extends Component {

    constructor(props) {
        super(props);

        this.comment = '';
    }

    componentDidMount() {
        const { id, getCommentList } = this.props;
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    setComment(event, data) {
        this.comment = data.value;
    }

    _addComment() {
        const { comment } = this;
        const { chunkIds, addComment } = this.props;

        addComment(chunkIds, comment);
    }

    mapCommentList(commentList) {
        return commentList
            .map(item => {
                return <Comment>
                    <Comment.Avatar src = { item.user.photo }/>
                    <Comment.Content>
                        <Comment.Author as='a'>{ item.user.nickName }</Comment.Author>
                        <Comment.Metadata>
                            <div>Today at 5:42PM</div>
                        </Comment.Metadata>
                        <Comment.Text>{ item.comment }</Comment.Text>
                    </Comment.Content>
                </Comment>
            })
    }

    render() {
        const { mapCommentList } = this;
        const { commentList } = this.props;
        const comments = mapCommentList(commentList);
        return (
            <div>
                <Comment.Group>
                    <div style = {{ height: '300px', overflowY: 'auto', margin: '0 0 50px 0' }}>
                    { comments }
                    </div>
                    <Form>
                        <Form.TextArea style = { { height: '100px' } } onChange={ (event, data) => this.setComment(event, data) }/>
                        <Button onClick={ () => this._addComment() } content='Добавить комментарий' icon='edit' color = 'blue'/>
                    </Form>
                </Comment.Group>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { commentList: state.posts.commentList ? state.posts.commentList : [] };
}

function mapDispatchToProps(dispatch) {
    return {
        getCommentList: id => dispatch(commentListGetByIdRequestAction(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentItem);