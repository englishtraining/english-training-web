import React, {Component} from "react";
import PostItem from "../PostItem";
import './CommentItem.css';
import {commentAddRequestAction, commentListGetByIdRequestAction} from "../../reducers/actions/post-actions";
import {connect} from "react-redux";

class CommentItem extends Component {

    styles = {
        postItem: {
            display: 'inline-block',
            cursor: 'pointer'
        },
        selectedItem: {
            background: '#2185d0',
            color: 'white',
            borderRadius: '5px 5px 5px 5px',
            padding: '2px 2px 2px 2px'
        },
        commentedItem: {
            background: 'rgb(33, 133, 150)',
            borderRadius: '5px 5px 5px 5px',
            padding: '2px 2px 2px 2px',
            color: 'white'
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            selected: false
        };
    }

    componentDidMount() {
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    _selectItem(event) {
        const { item, selectItem } = this.props;

        selectItem(event, item);
    }

    render() {
        const { item, selected } = this.props;
        const { postItem, selectedItem, commentedItem } = this.styles;

        let style = postItem;

        if (selected) {
            style = { ...postItem, ...selectedItem };
        } else if (item.commentList.length > 0) {
            style = { ...postItem, ...commentedItem };
        }

        return (
            <div style={ style } onClick = { (event, item) => this._selectItem(event, item) }>
                <PostItem item = { item }/>
            </div>
        );
    }
}

export default CommentItem;