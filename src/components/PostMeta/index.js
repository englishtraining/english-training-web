import React, {Component} from "react";
import {Container, Header, Image, Item} from "semantic-ui-react";

class PostMeta extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { title, photo } = this.props;

        return (
            <Container>
                <Header as='h2'>{ title }</Header>
                <Image src={ photo }/>
            </Container>
        );
    }
}

export default PostMeta;