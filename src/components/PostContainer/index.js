import React, {Component} from "react";
import { Grid } from "semantic-ui-react";
import { isMobile } from "react-device-detect";

class PostContainer extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    render() {
        const { divided, style, children } = this.props;
        return (
            <Grid style = { style } celled = { divided ? 'internally' : '' }>
                <Grid.Row>
                    <Grid.Column width = { isMobile ? 0 : 3 }>
                    </Grid.Column>
                    <Grid.Column width = { isMobile ? 16 : 10 }>
                        { children }
                    </Grid.Column>
                    <Grid.Column width = { isMobile ? 0 : 3 }>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default PostContainer;