import React, {Component} from 'react';
import {Dimmer, Item, Label, Loader, Rating} from 'semantic-ui-react'
import {Link} from "react-router-dom/umd/react-router-dom";
import {connect} from "react-redux";
import {postListRequestAction} from "../../reducers/actions/post-actions";
import {formatDate} from "../../service/UtilService";
import PostContainer from "../PostContainer";
import PostHeader from "../PostHeader";

class PostList extends Component {

    componentWillMount() {
        this.props.getPostList();
    }

    getItem(item) {
        return <Item>
            <Item.Image size='tiny' src={item.authorPhoto}/>
            <Item.Content>
                <Item.Header as='h2'>
                    <Link to={`/post/${item.postId}`}>{item.title}</Link>
                </Item.Header>
                <Item.Meta>{formatDate(new Date(item.date))}</Item.Meta>
                <Item.Description>
                    <Label size='mini'>Tag</Label>
                    <Label size='mini'>Tag</Label>
                    <Label size='mini'>Tag</Label>
                    <Label size='mini'>Tag</Label>
                </Item.Description>
                <Item.Extra>
                    <Rating icon='star' defaultRating={4} maxRating={5}/>
                </Item.Extra>
            </Item.Content>
        </Item>
    }

    getLoader() {
        return (
            <Dimmer inverted
                    active={this.props.postListFetching}>
                <Loader inverted
                        inline='centered'>Загрузка списка статей...</Loader>
            </Dimmer>
        )
    }

    render() {

        const {postList} = this.props;
        const listItems = postList.map(item => this.getItem(item));

        let links = [{url: `/posts`, name: 'Список публикации'}];

        return (
            <div>
                <PostContainer style={{margin: '0 0 50px 0'}}>
                    <PostHeader name='Публикации' links={links}/>
                </PostContainer>
                <PostContainer divided>
                    <div>
                        <Item.Group divided
                                    style={{margin: '50px'}}>
                            {listItems}
                        </Item.Group>
                        {this.getLoader()}
                    </div>
                </PostContainer>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        // Здесь происходит связывание свойств компонента и свойств объекта состояния хранилища Redux.
        //
        // Если исползуется всего один reducer, то свойства берутся как state.<property_name>.
        //
        // Если используется несколько reducer'ов, соединенных при помощи combineReducers(),
        // то свойства берутся как state.<reducer_block_name>.<property_name>.
        // <reducer_block_name> задается reducer'у при вызове combineReducers().
        postList: state.posts.postList ? state.posts.postList : [],
        postListFetching: state.posts.postListFetching,
        postListLastError: state.posts.postListLastError
    }
}

function mapDispatchToProps(dispatch) {
    return {
        // Здесь происходит связывание свойств компонента и действий Redux
        getPostList: () => dispatch(postListRequestAction())
    }
}

// Подключение компонента к хранилищу состояний Redux
export default connect(mapStateToProps, mapDispatchToProps)(PostList);