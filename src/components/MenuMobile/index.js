import React, {Component} from "react";
import {connect} from "react-redux";
import {Image, Menu} from "semantic-ui-react";
import {Link} from "react-router-dom";
import {authCookieRequestAction, authSignOutAction} from "../../reducers/actions/auth-actions";

class MenuMobile extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    getLoginMenuItem() {
        const user = this.props.user;

        const menu = [<Menu.Item key='addPostMobileMenuItem' position='right'>
            <Link to="/add-post">Добавить</Link>
        </Menu.Item>,
            <Menu.Item key='postListMobileMenuItem'>
                <Link to="/posts">Публикации</Link>
            </Menu.Item>];

        if (user) {
            return [<Menu.Item key='avatarMobileMenuItem' style={{display: 'inline-flex'}}>
                <Image circular src={user.photo}/>
            </Menu.Item>,
                menu,
                <Menu.Item key='signOutMobileMenuItem'
                           name='Выход'
                           onClick={() => this.props.signOut()}/>];
        } else {
            return [<Menu.Item key='signInMobileMenuItem'
                               name='Войти'
                               onClick={() => this.props.showLoginDialog()}/>,
                menu];
        }
    }

    render() {
        return this.getLoginMenuItem();
    }
}

function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        tryAuthByCookie: (cookie) => dispatch(authCookieRequestAction(cookie)),
        signOut: () => dispatch(authSignOutAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuMobile)