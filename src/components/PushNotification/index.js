import React, {Component} from 'react'
import {Icon, Message} from 'semantic-ui-react'

export default class PushNotification extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: true
        };
    }

    componentDidMount() {
        const {messageId, duration, onClose} = this.props;

        if (duration !== 0) {
            setTimeout(() => {
                this.setState({visible: false});
                onClose(messageId);
            }, duration);
        }
    }

    render() {
        const {visible} = this.state;
        const {color, icon, message} = this.props;

        return (
            <Message visible={visible}
                     style={{width: '300px', zIndex: 2222}}
                     color={color}
                     icon>
                <Icon name={icon} size='big'/>
                <Message.Content>
                    {message}
                </Message.Content>
            </Message>
        )
    }
}