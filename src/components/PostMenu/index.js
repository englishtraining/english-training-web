import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Container, Icon, Image, Input, Menu, Segment} from "semantic-ui-react";
import {isMobile} from "react-device-detect";
import {Link} from "react-router-dom";
import {authCookieRequestAction, authSignOutAction} from "../../reducers/actions/auth-actions";

class PostMenu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeItem: 'home'
        }
    }

    componentDidMount() {
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    getLoginMenuItem() {
        const user = this.props.user;

        if (isMobile) {
            return <Menu.Item key='mobileMenuItem' style={{width: '50px'}}>
                <Icon style={{color: 'white'}}
                      name='bars'
                      link
                      onClick={() => this.props.showMenu()}
                      size={'large'}/>
            </Menu.Item>;
        }

        if (user) {
            return [<Menu.Item key='avatarMenuItem' style={{width: '50px'}}>
                <Image circular src={user.photo}/>
            </Menu.Item>,
                <Menu.Item key='signOutMenuItem' style={{width: '50px'}}>
                    <Icon style={{color: 'white'}}
                          name='sign out'
                          link
                          onClick={() => this.props.signOut()}
                          size={'large'}/>
                </Menu.Item>]
        } else {
            return <Menu.Item key='signInMenuItem' style={{width: '50px'}}>
                <Icon style={{color: 'white'}}
                      name='sign in'
                      link
                      onClick={() => this.props.showLoginDialog()}
                      size={'large'}/>
            </Menu.Item>
        }
    }

    render() {
        return <Segment inverted style={{padding: '0 0 0 0', margin: '0 0 0 0'}} stacked>
            <Container textAlign='justified'>
                <Menu inverted secondary>
                    <Menu.Menu position='left'>
                        <Menu.Item key='homeMenuItem'>
                            <Button circular icon='home' onClick={() => window.location = '/'}/>
                        </Menu.Item>
                        {!isMobile ? [<Menu.Item key='addPostMenuItem'>
                            <Link to="/add-post">Добавить</Link>
                        </Menu.Item>,
                            <Menu.Item key='postListMenuItem'>
                                <Link to="/posts">Публикации</Link>
                            </Menu.Item>] : null}
                    </Menu.Menu>
                    <Menu.Menu position='right'>
                        <Menu.Item key='searchMenuItem'>
                            <Input icon='search' placeholder='Поиск'/>
                        </Menu.Item>
                        {this.getLoginMenuItem()}
                    </Menu.Menu>
                </Menu>
            </Container>
        </Segment>;
    }
}

function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        tryAuthByCookie: (cookie) => dispatch(authCookieRequestAction(cookie)),
        signOut: () => dispatch(authSignOutAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostMenu)