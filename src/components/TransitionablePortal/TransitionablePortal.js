import {TransitionablePortal} from "semantic-ui-react";

class TransitionablePortalEnglish extends TransitionablePortal {

    constructor(props) {
        super(props);

        this.handleTransitionHide = () => {};
        this.handlePortalClose = () => {};
    }
}

export default TransitionablePortalEnglish;