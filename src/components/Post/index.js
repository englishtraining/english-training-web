import React, { Component } from 'react';
import CommentItem from '../CommentItem';
import Author from '../Author';
import {Button, Container, Item, Rail} from "semantic-ui-react";
import TransitionablePortalEnglish from "../TransitionablePortal/TransitionablePortal";
import { connect } from "react-redux";
import PostMeta from "../PostMeta";
import ShowComment from "../ShowComment";
import AddComment from "../AddComment";
import LineButton from "../LineButton";
import PostContainer from "../PostContainer";
import PostHeader from "../PostHeader";

class Post extends Component {

    styles = {
        ctrlButton: {
            position: 'fixed',
            bottom: '10px',
            left: '10px',
            fontSize: '30px',
            color: 'black'
        },
        commentButton: {
            position: 'fixed',
            bottom: '10px',
            right: '10px',
            fontSize: '30px',
            color: 'black'
        }
    };

    constructor(props) {
        super(props);

        this.ctrlKey = false;
        this.itemList = [];
        this.commentItems = [];
        this.events = [];
        this.commentListValue = [];

        this.state = {
            selectedItems: [],
            commentList: [],
            showedLineButton: false,
            showedAddComment: false,
            showedComment: false,
            ctrlButton: false,
            lineButtonA: 0,
            lineButtonB: 0,
            desktop: true,
        };
    }

    componentDidMount() {
        document.addEventListener('keydown', (event) => this.eventListener(event));
        document.addEventListener('keyup', (event) => this.eventListener(event));
        document.addEventListener('click', () => this.documentShowLineButton());
    }

    componentWillMount() {
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', (event) => this.eventListener(event));
        document.removeEventListener('keyup', (event) => this.eventListener(event));
        document.removeEventListener('click', () => this.documentShowLineButton());
    }


    eventListener(event) {
        this.ctrlKey = event.ctrlKey;
    }

    mapItem(items, comments) {
        const { selectedItems } = this.state;

        this.itemList = [];

        return items
                .map(item => {
                    if (item.chunkId !== null) {
                        item.commentList = this.mapComment(item.chunkId, comments);

                        let commentItem = <CommentItem item = { item } selected = { selectedItems.find(selected => selected.chunkId === item.chunkId) ? true : false } selectItem = { (event, item) => this.commentItem(event, item) }/>;

                        this.commentItems[item.chunkId] = commentItem;

                        this.itemList[item.chunkId] = item;

                        return commentItem;
                    } else if (item.text.charCodeAt() === 10) {
                        return <br/>;
                    } else {
                        return <span>{ item.text }</span>;
                    }
                });
    }

    mapComment(id, comments) {
        return comments
            .filter(item => item.chunkIds
                .find(item => item === id));
    }

    commentItem(event, item) {
        const { selectedItems } = this.state;
        const { ctrlKey } = this;

        if (ctrlKey) {
            let selectedItemList = selectedItems.filter(selected => selected.chunkId !== item.chunkId);

            if (selectedItemList.length !== selectedItems.length) {
                this.events = this.events.filter(event => event.chunkId !== item.chunkId);

                if (selectedItemList.length === 0) {
                    this.showLineButton(false, 0, 0);
                } else {
                    let previousEvent = this.events[this.events.length - 1];

                    this.showLineButton(true, previousEvent.a, previousEvent.b);
                }

                this.setState({ selectedItems: selectedItemList });
            } else {
                let a = event.target.offsetLeft + (event.target.offsetWidth / 2) + 310;
                let b = event.target.offsetTop +  150;

                selectedItems.push(item);

                this.setState({ selectedItems });

                this.showLineButton(true, a, b);

                if (this.events.length === 0 || !this.events.find(event => event.chunkId === item.chunkId)) {
                    this.events.push({ chunkId: item.chunkId, a: a, b: b });
                }
            }
        } else {
            if (item.commentList && item.commentList.length > 0) {
                this.setState({ commentList: item.commentList }, () => this.showComment());
            } else {
                this.setState({ commentList: [] });
            }
        }
    }

    showLineButton(showed, a, b) {
        this.setState({ showedLineButton: showed, lineButtonA: a, lineButtonB: b });
    }

    showAddComment() {
        const { showedAddComment, selectedItems } = this.state;

        this.ctrlKey = false;

        let _selectedItems = selectedItems;

        if (showedAddComment) {
            _selectedItems = [];
        }

        this.setState({ showedAddComment: !showedAddComment, showedComment: false, selectedItems: _selectedItems, ctrlButton: false });
    }

    showComment() {
        const { showedComment } = this.state;

        this.ctrlKey = false;

        this.setState({ showedComment: !showedComment, showedAddComment: false, ctrlButton: false });
    }

    documentShowLineButton() {
        const { ctrlKey } = this;

        if (!ctrlKey) {
            this.showLineButton(false, 0, 0);
        }
    }

    onCtrlButton() {
        const { ctrlButton } = this.state;

        this.ctrlKey = !this.ctrlKey;

        this.setState({ ...this.state, ctrlButton: !ctrlButton });
    }

    render() {
        const { postId, items, authorName, authorPhoto, title, date, comments } = this.props;
        const { selectedItems, commentList, showedLineButton, showedAddComment, showedComment, ctrlButton, lineButtonA, lineButtonB } = this.state;
        const { itemList, styles, commentListValue } = this;

        let postItems = this.mapItem(items, comments);
        let data = new Date(date);
        let links = [ { url: `/post/${postId}`, name: title } ];

        data = data.getDay() + '-' + +(data.getMonth() + 1) + '-' + data.getFullYear();

        return (
            <div>
                <PostContainer style = {{ margin: '0 0 50px 0' }}>
                    <PostHeader name = 'Публикация' links = { links }/>
                </PostContainer>
                <PostContainer divided>
                    <div>
                        <Container text textAlign='left' style = { { position: 'relative' } }>
                            <Item.Group divided>
                                <Author authorPhoto = { authorPhoto } authorName = { authorName } data = { data }/>
                            </Item.Group>
                        </Container>
                        <Container text textAlign='center'>
                            <PostMeta title = { title } photo = { '' }/>
                        </Container>
                        <Container text textAlign='justified'>
                            { postItems }
                        </Container>
                        <AddComment postId = { postId } selectedItems = { selectedItems } showedAddComment = { showedAddComment } showAddComment = { () => this.showAddComment() }/>
                        <ShowComment items = { itemList } commentList = { commentList } showedComment = { showedComment } commentListValue = { commentListValue } showComment = { () => this.showComment() }/>
                        <LineButton showedLineButton = { false } lineButtonA = { lineButtonA } lineButtonB = { lineButtonB } showAddComment = { () => this.showAddComment() }/>
                        <TransitionablePortalEnglish open = { true }>
                            <Button toggle active = { ctrlButton } style={ styles.ctrlButton }
                                    onClick={ () => this.onCtrlButton() } circular
                                    icon='connectdevelop'/>
                        </TransitionablePortalEnglish>
                        <TransitionablePortalEnglish open = { selectedItems.length > 0 }>
                            <Button style={ styles.commentButton }
                                    onClick={ () => this.showAddComment() } circular
                                    icon='write'/>
                        </TransitionablePortalEnglish>
                    </div>
                </PostContainer>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { user: state.auth.user };
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);