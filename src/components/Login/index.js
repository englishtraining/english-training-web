import React, {Component} from "react";
import {Button, Container, Header, Modal} from "semantic-ui-react";

class Login extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillMount() {
    }

    componentWillUnmount() {
    }

    login(type) {
        const {close} = this.props;

        if (type === 'vk') {
            document.getElementsByClassName('ulogin-button-vkontakte')[0].click();

            this.setState({showedLoginDialog: false});
        }

        close();
    }

    render() {
        const {showed, close} = this.props;

        return <Modal
            open={showed}
            onClose={() => close()}
            size='small'
            style={{top: '100px'}}>
            <Header icon='sign in' content='Авторизация'/>
            <Modal.Content>
                <Header as='h2'>Вы можете зайти на сайт через социальные сети</Header>
                <Container className='login' textAlign='center'>
                    <Button circular color='vk' icon='vk' size='large' onClick={() => this.login('vk')}/>
                    <Button circular color='facebook' icon='facebook' size={'large'}/>
                    <Button circular color='red' icon='google' size={'large'}/>
                    <Button circular color='youtube' icon='youtube' size={'large'}/>
                </Container>
            </Modal.Content>
        </Modal>;
    }
}

export default Login;