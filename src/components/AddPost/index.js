import React, {Component} from 'react';
import { connect } from "react-redux";
import { Container, Header, Button, Form, TextArea, Loader } from 'semantic-ui-react'
import { postAddRequestAction } from "../../reducers/actions/post-actions";
import  './AddPost.css'

class AddPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            content: '',           
            originalLink: ''
        };
    };

    addPost = () => {
        let { title, content} = this.state;
        
        if ((title.length && content.length) === 0) {
            alert('Не все обязательные поля заполнены');
        } else {                
            this.props.sendPost({...this.state});
        }
    };

    render() {
        return (            
            <Container text className="add-post">
                
                <div style={{display: this.props.load ? 'flex' : 'none'}}
                    className="add-post-loader">
                    <Loader active inline='centered' size='massive' />
                </div>

                <Header as='h2'>Добавление статьи</Header>
                <Form>
                    <Form.Field>
                        <label>Название</label>
                        <input placeholder={'Поле обязательное для заполения'}
                               style={{background: this.state.title.length === 0 ? 'antiquewhite' : ''}}
                               onChange={e => this.setState({title: e.target.value})}
                               value={this.state.title}/>
                    </Form.Field>
                    <Form.Field>
                        <label>Текст</label>
                        <TextArea placeholder={'Поле обязательное для заполения'}
                                  style={{background: this.state.content.length === 0 ? 'antiquewhite' : ''}}
                                  onChange={e => this.setState({content: e.target.value})}
                                  value={this.state.content}/>
                    </Form.Field>
                    <Form.Field>
                        <label>Ссылка на оригинал</label>
                        <input placeholder='Ссылка на оригинал статьи'
                               onChange={e => this.setState({originalLink: e.target.value})}
                               value={this.state.originalLink}/>
                    </Form.Field>
                    {/* <Form.Field>
                        <label>Ссылка на картинку</label>
                        <input placeholder='Ссылку на картинку'
                               onChange={e => this.setState({photo: e.target.value})}
                               value={this.state.photo}/>
                    </Form.Field> */}

                    <Button type='submit' onClick={this.addPost}>Добавить</Button>
                </Form>
            </Container>
        );
    };
}

function mapStateToProps(state) {
    return { user: state.auth.user, load: state.posts.postAddFetching};
}

function mapDispatchToProps(dispatch) {
    return {
        sendPost: post => dispatch(postAddRequestAction(post))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPost);