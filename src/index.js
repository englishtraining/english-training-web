import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from "react-redux";
import './polyfill/array-find';
import {store} from "./reducers";
import {authTokenRequestAction} from "./reducers/actions/auth-actions";

// Задаем реальный обработчик авторизации по токену
window.tokenAction = (token) => {
    store.dispatch(authTokenRequestAction(token));
};

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root'));

registerServiceWorker();
