import React, {Component} from 'react';
import {Menu, Segment, Sidebar} from 'semantic-ui-react'
import './App.css';
import PostList from "./components/PostList";
import AddPost from "./components/AddPost";
import Post from "./containers/MyPost";
import Home from "./containers/Home";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {authCookieName, getCookie} from "./service/CookieService";
import {connect} from "react-redux";
import {authCookieRequestAction, authSignOutAction} from "./reducers/actions/auth-actions";
import PostMenu from "./components/PostMenu";
import MenuMobile from "./components/MenuMobile";
import Login from "./components/Login";
import {PushNotificator} from "./containers/PushNotificator";

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            login: false,
            showedLoginDialog: false,
            showedMenu: false
        }
    }

    componentWillMount() {
        this.tryAuthByCookie();

        document.addEventListener('click', event => {
            // console.log(event);

            if (event.target.className.indexOf('login') < 0) {
                this.setState({...this.state, login: false});
            }
        }, false);
    }

    componentWillUnmount() {
        document.addEventListener('click', (event) => console.log(event), false);
    }

    setVisible(visible) {
        if (visible || this.state.visible) {
            this.setState({
                visible: false
            });
        } else {
            this.setState({
                visible: true
            });
        }
    }

    // Поиск куки авторизации и, при наличии таковой, попытка авторизации по куке
    tryAuthByCookie() {
        const user = this.props.user;

        if (!user) {
            let cookie = getCookie(authCookieName);

            if (cookie) {
                this.props.tryAuthByCookie(cookie);
            }
        }
    }

    showMenu() {
        this.setState({showedMenu: !this.state.showedMenu});
    }

    showLoginDialog() {
        this.setState({showedLoginDialog: !this.state.showedLoginDialog});
    }

    closeMenu() {
        this.setState({showedMenu: false});
    }

    closeLoginDialog() {
        this.setState({showedLoginDialog: false});
    }

    render() {
        const {showedMenu, showedLoginDialog} = this.state;

        return (
            <div className="App">
                <div onClick={() => this.setVisible(true)}>
                    <BrowserRouter style={{margin: '20px 0 0 0', height: '100%'}}>
                        <div>
                            <PostMenu showMenu={() => this.showMenu()} showLoginDialog={() => this.showLoginDialog()}/>
                            <Segment basic style={{margin: '0 0 0 0', height: '100%'}}>
                                <PushNotificator style={{margin: '13px 5px 0 0'}}
                                                 position='right'/>
                                <Sidebar.Pushable>
                                    <Sidebar
                                        as={Menu}
                                        direction='right'
                                        animation='overlay'
                                        icon='labeled'
                                        inverted
                                        onHide={() => this.closeMenu()}
                                        vertical
                                        visible={showedMenu}
                                        width='thin'>
                                        <MenuMobile showLoginDialog={() => this.showLoginDialog()}/>
                                    </Sidebar>
                                    <Sidebar.Pusher dimmed={showedMenu}>
                                        <Switch>
                                            <Route exact path='/' component={Home}/>
                                            <Route exact path='/posts' component={PostList}/>
                                            <Route exact path='/post/:postId' render={() => (
                                                <Post/>
                                            )}/>
                                            <Route exact path='/add-post' component={AddPost}/>
                                        </Switch>
                                    </Sidebar.Pusher>
                                </Sidebar.Pushable>
                            </Segment>
                            <Login showed={showedLoginDialog} close={() => this.closeLoginDialog()}/>
                        </div>
                    </BrowserRouter>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        tryAuthByCookie: (cookie) => dispatch(authCookieRequestAction(cookie)),
        signOut: () => dispatch(authSignOutAction())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);