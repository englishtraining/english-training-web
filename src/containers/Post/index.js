import React, {Component} from 'react';
import {
    Button,
    Comment,
    Container,
    Divider,
    Form,
    Header,
    Image,
    Item,
    Label,
    Rating,
    Segment,
    Tab
} from 'semantic-ui-react'
import './Post.css';
import Alert from 'react-s-alert';
import Platform from 'react-platform-js'

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import TransitionablePortalEnglish from "../../components/TransitionablePortal/TransitionablePortal";
import {postGetRequestAction} from "../../reducers/actions/post-actions";
import {connect} from "react-redux";

class Post extends Component {

    state = {
        post: {
            header: '',
            text: '',
            photo: '',
            data: '',
            tag: [],
            user: {
                name: '',
                photo: ''
            }
        },
        comment: [],
        commentValue: '',
        html: [],
        commentVisible: false,
        commentMenu: [],
        commentMenuVisible: false,
        ctrlKey: false,
        id: 0,
        user: {
            name: '',
            photo: '',
            star: 0
        },
        htmlItem: null
    };

    constructor(props) {
        super(props);

        Array.prototype.remove = function (value) {
            let id = this.indexOf(value);

            if (id !== -1) {
                return this.splice(id, 1);
            }

            return false;
        };
    }

    componentDidMount() {


        let post = {
            header: 'Станции петербургского метро ранжировали по стоимости квартир',
            text: 'Самая дорогая петербургская недвижимость продается около станции метро «Крестовский остров», свидетельствуют материалы федерального портала «Мир квартир», поступившие в «Дом» в понедельник, 2 апреля.\n' +
            '\n' +
            'Средняя цена квадратного метра на «Крестовском» более чем в два раза отличается от средней по городу и составляет 280 тысяч рублей (без учета элитных предложений). Купить квартиру здесь можно примерно за 25,6 миллиона рублей.\n' +
            '\n' +
            'На втором месте по стоимости жилья — станция «Чернышевская», расположенная в Центральном районе Петербурга, недалеко от Таврического сада. «Местные новостройки, тщательно вписанные в историческую застройку, пользуются повышенным спросом — во многом потому, что район находится чуть в стороне от оживленного Невского проспекта», — поясняют аналитики. Квадратный метр у данной станции оценивается в 197 тысяч рублей, среднестатистическая квартира — почти в 15 миллионов рублей.\n' +
            '\n' +
            'МАТЕРИАЛЫ ПО ТЕМЕ\n' +
            '\n' +
            'Это чисто Питер\n' +
            'Сколько стоят квартиры в воспетом Шнуром городе на Неве\n' +
            'Далее в рейтинге располагаются подряд сразу четыре станции Петроградской стороны, обслуживающие соседние районы, — «Спортивная», «Горьковская», «Чкаловская» и «Петроградская». Квадратные метры около них стоят 176 тысяч, 175,9 тысячи, 172 тысячи и 165 тысяч рублей соответственно.\n' +
            '\n' +
            'Минимальные для Петербурга цены на квартиры зафиксированы около конечной станции Кировско-Выборгской линии в северном направлении — «Девяткино». Она обслуживает одноименный микрорайон, который, как отмечают эксперты, «возводится в чистом поле». Средняя цена местного квадратного метра — 78,7 тысячи рублей, квартира в среднем обойдется в 4,5 миллиона рублей.\n' +
            '\n' +
            'Также недорогую недвижимость можно найти около станций «Рыбацкое», «Обухово», «Гражданский проспект» и «Парнас».\n' +
            '\n' +
            'В Москве самое дорогое жилье расположено в районах у станций «Охотный ряд», «Театральная», «Площадь революции» и «Кропоткинская». Самые дешевые квартиры — у станций метро за МКАД',
            photo: 'http://icdn.lenta.ru/images/2018/04/02/01/20180402012440952/pic_51133155c796c8549ee6d3d4d71b6a5a.jpg',
            data: '02.04.2018',
            tag: ['метро', 'Санкт Петербург'],
            user: {
                name: 'Имя Фамилия',
                photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvRadXKIp_bFbqpx44vXaRlJMaXuQ1p08ATk1MnsVem02HwqXAjQ'
            }
        };
        let user = {
            name: 'Имя Фамилия',
            photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvRadXKIp_bFbqpx44vXaRlJMaXuQ1p08ATk1MnsVem02HwqXAjQ'
        };

        this.setState({...this.state, post: this.setDivPost(post), user: user});
    }

    componentWillMount() {
        document.addEventListener('keydown', (event) => this.eventListener(event), false);
        document.addEventListener('keyup', (event) => this.eventListener(event), false);
                let url = window.location.href;
                let postId = url.substring(url.lastIndexOf('/') + 1);

                this.getPostById(postId);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', (event) => this.eventListener(event), false);
        document.removeEventListener('keyup', (event) => this.eventListener(event), false);
    }

    eventListener(event) {
        this.setState({...this.state, ctrlKey: event.ctrlKey});
    }

    mouseEventListener(event) {
        let html = this.state.html;

        if (event.target.className === 'item') {
            event.target = event.target.offsetParent;
        }

        if (this.state.ctrlKey) {
            if (!event.target.classList.contains('item-touch')) {
                html.push(event.target);

                event.target.classList.add('item-touch');
            } else if (event.target.classList.contains('item-touch')) {
                html.remove(event.target);

                event.target.classList.remove('item-touch');
            }

            if (html.length > 0) {
                html[html.length - 1].appendChild(document.getElementsByClassName('post-write')[0]);
            }

            this.setFilter();
        } else if (this.state.commentVisible) {
            this.setState({...this.state, commentVisible: false, id: 0});
        } else if (!this.state.commentMenuVisible) {
            this.setFilter();

            this.setCommentMenu(event.target);
        } else if (this.state.commentMenuVisible) {
            this.setFilter();

            this.setState({...this.state, commentMenuVisible: false, id: 0});
        }
    }

    setPost(post) {
        this.setState({...this.state, post});
    }

    setFilter() {
        Array.prototype.forEach.call(document.getElementsByClassName('post-item'), (item) => item.classList.remove('filter'));
    }

    setDivPost(post) {
        let item = post.text.split(' ');

        post.text = item.map(value => <div className="post-item" comment=""
                                           onClick={(event) => this.mouseEventListener(event)}>{value}&nbsp;</div>);

        return post;
    }

    setVisiblePopup() {
        if (this.state.html.length > 0) {
            this.setState({...this.state, commentVisible: true, commentMenuVisible: false, ctrlKey: false});
        } else {
            Alert.error('Вы не выделили текст', {
                position: 'bottom-right',
                effect: 'slide',
                timeout: 2222
            });
        }
    }

    setCommentMenu(html) {
        let data = html.getAttribute('comment');
        let menuData = this;

        if (data.length > 0) {
            let idList = data.split(' ');

            let value = idList.map(item => {
                if (item.length > 1) {
                    let filter = Array.prototype.filter.call(document.getElementsByClassName('post-item'), (postItem) => postItem.getAttribute('comment').indexOf(item) > -1);
                    let name = this.getName(filter);
                    let id = menuData.getId(filter);
                    let comment = menuData.state.comment[id].map(item => {
                        return <Comment>
                            <Comment.Avatar as='a' src={this.state.user.photo}/>
                            <Comment.Content>
                                <Comment.Author>{this.state.user.name}</Comment.Author>
                                <Comment.Metadata>
                                    <div>{new Date().toDateString()}</div>
                                </Comment.Metadata>
                                <Comment.Text>
                                    {item.value}
                                </Comment.Text>
                            </Comment.Content>
                        </Comment>
                    });

                    Array.prototype.filter.call(filter, (filterItem) => filterItem.classList.add('filter'));

                    return {id: item, menuItem: name, item: <Comment.Group>{comment}</Comment.Group>}
                }
            });

            this.setState({...this.state, commentMenu: value, commentMenuVisible: true, htmlItem: html});
        }
    }

    setCommentValue(event, value) {
        this.setState({...this.state, commentValue: value.value});
    }

    setCommentId() {
        let item = this.state.html;
        let id = this.getId(item);

        if (!id) {
            id = new Date().getTime();
        }

        this.setCommentDiv(id, item);
    }

    setCommentDiv(id) {
        let item = this.state.html;
        let a = this.state.comment;

        if (!a[id]) {
            a[id] = [];
        }

        a[id].push({name: this.getName(item), value: this.state.commentValue});

        item.forEach(a => {
            let value = a.getAttribute('comment');

            value = value.replace(id, '');

            a.setAttribute('comment', value ? value + ' ' + id : id);
            a.classList.add('comment');
            a.classList.remove('item-touch');

            let b = (250 - (value.split(' ').length * 40));

            a.style.borderBottomColor = 'rgb(' + b + ', ' + b + ',' + b + ')';
        });

        this.setState({...this.state, comment: a, commentVisible: false, html: [], commentValue: ''});
    }

    getTag(item) {
        if (item && item.length > 0) {
            return item.map(a => <Label>{a}</Label>);
        }
    }

    getId(item) {
        let name = this.getName(item);
        let menuData = this;

        return item.map(a => {
            let value = a.getAttribute('comment');

            return value.split(' ');
        }).reduce((b, c) => {
            return b.filter(d => c.indexOf(d) > -1);
        }).filter(a => {
            if (menuData.state.comment[a]) {
                return menuData.state.comment[a][0].name.trim() === name.trim();
            } else {
                return false;
            }
        })[0];
    }

    getName(item) {
        return item.map(a => a.innerText).join('')
    }

    getPostById(postId) {
        this.props.getPost(postId);
    }

    render() {
        console.log(this.props.postGet);
        let {post, commentVisible, commentMenuVisible, commentMenu, id, htmlItem, commentValue, ctrlKey, html} = this.state;
        let blue = 'blue';

        return (
            <div>
                <TransitionablePortalEnglish style={{overflow: 'hidden'}} open={commentVisible}>
                    <Segment style={{height: '300px', width: '100%', position: 'fixed', bottom: '0', zIndex: 2222}}>
                        <Form>
                            <Form.TextArea style={{height: '200px'}} value={commentValue}
                                           onChange={(event, value) => this.setCommentValue(event, value)}/>
                            <Button onClick={() => this.setCommentId()} content='Добавить комментарий' icon='edit'
                                    primary/>
                        </Form>
                    </Segment>
                </TransitionablePortalEnglish>
                <TransitionablePortalEnglish style={{overflow: 'hidden'}} open={commentMenuVisible}>
                    <Segment style={{
                        height: '500px',
                        width: '100%',
                        overflow: 'hidden',
                        position: 'fixed',
                        bottom: '0',
                        zIndex: 2222
                    }}>
                        <div style={{backgroundColor: '#2185d0', width: '100%'}}>
                            <Tab style={{overflowX: 'auto', display: 'inline-flex', width: '100%'}} activeIndex={id}
                                 menu={{color: 'blue', tabular: false, inverted: true}} panes={commentMenu}
                                 onTabChange={(event, item) => this.setState({...this.state, id: item.activeIndex})}/>
                        </div>
                        <div style={{overflowY: 'auto', margin: '10px 0 20px 0', height: '60%'}}>
                            {commentMenu[id] ? commentMenu[id].item : ''}
                        </div>
                        <Form>
                            <Form.TextArea style={{height: '20%'}} value={commentValue}
                                           onChange={(event, value) => this.setCommentValue(event, value)}/>
                            <Button onClick={() => {
                                this.setCommentDiv(commentMenu[id].id);
                                this.setCommentMenu(htmlItem);
                            }} content='Добавить комментарий' icon='edit' primary/>
                        </Form>
                    </Segment>
                </TransitionablePortalEnglish>
                <Container textAlign='left'>
                    <Item.Group divided>
                        <Item>
                            <Image src={post.user.photo} circular width='100px' height='100px'/>
                            <Item.Content>
                                <Item.Header as='a'>{post.user.name}</Item.Header>
                                <Item.Meta>
                                    <Rating icon='star' defaultRating={post.user.star}
                                            maxRating={5}/>
                                </Item.Meta>
                                <Item.Meta>
                                    Дата публикации: {post.data}
                                </Item.Meta>
                                <Item.Extra>
                                    {this.getTag(post.tag)}
                                </Item.Extra>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Container>
                <Divider/>
                <Container text textAlign='center'>
                    <Header as='h4' className='post-header'>{post.header}</Header>
                    <Image src={post.photo}/>
                </Container>
                <Container text textAlign='justified'>
                    {post.text}
                </Container>
                <Platform rules={{DeviceType: 'mobile'}}>
                    <TransitionablePortalEnglish open={true}>
                        <Button style={{
                            position: 'fixed',
                            bottom: '10px',
                            left: '10px',
                            fontSize: '30px',
                            color: ctrlKey ? 'blue' : 'black'
                        }} className='ctrl' onClick={() => this.setState({...this.state, ctrlKey: !ctrlKey})} circular
                                icon='connectdevelop'/>
                    </TransitionablePortalEnglish>
                </Platform>
                <div className='post-write' style={{
                    position: 'absolute',
                    bottom: '30px',
                    zIndex: 2222,
                    display: html.length > 0 ? 'flex' : 'none',
                    left: '50%',
                    transform: 'translateX(-50%)'
                }}>
                    <Button onClick={() => this.setVisiblePopup()} circular icon='write'></Button>
                    <Button circular icon='tag'></Button>
                    <Button circular icon='plus'></Button>
                    <Button circular icon='help circle'></Button>
                </div>
                <Alert stack={{limit: 3}}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        // Здесь происходит связывание свойств компонента и свойств объекта состояния хранилища Redux.
        //
        // Если исползуется всего один reducer, то свойства берутся как state.<property_name>.
        //
        // Если используется несколько reducer'ов, соединенных при помощи combineReducers(),
        // то свойства берутся как state.<reducer_block_name>.<property_name>.
        // <reducer_block_name> задается reducer'у при вызове combineReducers().
        postGet: state.posts.postGet,
        postGetFetching: state.posts.postGetFetching,
        postGetLastError: state.posts.postGetLastError
    }
}

function mapDispatchToProps(dispatch) {
    return {
        // Здесь происходит связывание свойств компонента и действий Redux
        getPost: (postId) => dispatch(postGetRequestAction(postId)),
    }
}

// Подключение компонента к хранилищу состояний Redux
export default connect(mapStateToProps, mapDispatchToProps)(Post);
