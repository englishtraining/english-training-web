import React, { Component } from 'react';
import './MyPost.css';
import { postGetRequestAction } from "../../reducers/actions/post-actions";
import {connect} from "react-redux";
import Post from '../../components/Post';


class MyPost extends Component {

    postId = undefined;

    componentDidMount() {

    }

    componentWillMount() {
        let url = window.location.href;

        this.postId = url.substring(url.lastIndexOf('/') + 1);

        this.getPostById(this.postId);
    }

    componentWillUnmount() {
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.commentList !== nextProps.commentList) {
            this.getPostById(this.postId);
        }
    }

    getPostById(postId) {
        this.props.getPost(postId);
    }

    render() {
        const { postId,  content, title, authorNickName, authorPhoto, date, comments } = this.props.postGet;

        return (
            <div>
               <Post postId = { postId } items = { content } authorName = { authorNickName } authorPhoto = { authorPhoto } title = { title } date = { date } comments = { comments }/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        postGet: state.posts.postGet ?
            state.posts.postGet :
            {
                authorFirstName: null,
                authorId: null,
                authorLastName: null,
                authorNickName: null,
                authorPhoto: null,
                comments: [],
                content: [],
                cookie: null,
                originalLink: null,
                postId: null,
                title: null,
                date: null
            },
        postGetFetching: state.posts.postGetFetching,
        postGetLastError: state.posts.postGetLastError,
        commentList: state.posts.commentAdd
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getPost: (postId) => dispatch(postGetRequestAction(postId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPost);