import React, {Component} from "react";
import {List, Rail, Transition} from "semantic-ui-react";
import connect from "react-redux/es/connect/connect";
import {removeMessageAction} from "../../reducers/actions/push-notification-actions";
import PushNotification from "../../components/PushNotification";

class PushNotificationContainer extends Component {

    constructor(props) {
        super(props);
    }

    renderMessages() {
        return this.props.messages.map(message => this.renderMessage(message));
    }

    renderMessage(message) {
        return (
            <List.Item key={message.messageId}>
                <PushNotification messageId={message.messageId}
                                  color={message.color}
                                  icon={message.icon}
                                  duration={message.duration}
                                  message={message.message}
                                  onClose={this.props.removeMessage}/>
            </List.Item>
        );
    }

    render() {
        let {position, style} = this.props;
        return (
            <Rail attached
                  internal
                  position={position}
                  style={style}>
                <Transition.Group as={List}
                                  animation='fade up'
                                  duration={750}>
                    {this.renderMessages()}
                </Transition.Group>
            </Rail>
        );
    }
}

function mapStateToProps(state) {
    return {
        messages: state.pushNotification.messages
    }
}

function mapDispatchToProps(dispatch) {
    return {
        removeMessage: (messageId) => dispatch(removeMessageAction(messageId))
    }
}

export const PushNotificator = connect(mapStateToProps, mapDispatchToProps)(PushNotificationContainer);