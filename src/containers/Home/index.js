import React, {Component} from 'react';
import {Container, List, Message} from 'semantic-ui-react';

class Home extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container textAlign='left'>
                <Message info size='large'>
                    <Message.Header>Добро пожаловать</Message.Header>
                    <p>сайт English training поможет вам сделать,</p>
                    <p>то что вы давно хотели</p>
                </Message>
            </Container>
        );
    }
}

export default Home;
