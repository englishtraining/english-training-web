const RequestType = {
    ULOGIN_AUTH: 'uLoginAuth',
    COOKIE_LOGIN_AUTH: 'cookieLoginAuth',
    POST_LIST: 'getAllPosts',
    POST_ADD: 'newPost',
    POST_GET: 'getPost',
    COMMENT_ADD: 'newComment',
    COMMENT_LIST_GET_BY_ID: 'getComments',
    COMMENT_LIST_GET: 'commentList'
};

export {RequestType};