import {ActionType} from "./actions/action-type";

const initialState = {
    postListFetching: false,
    postList: undefined,
    postListLastError: undefined,
    postAddFetching: false,
    postAdd: undefined,
    postAddLastError: undefined,
    commentAddFetching: false,
    commentAdd: undefined,
    commentAddLastError: undefined,
    commentList: undefined
};

export const postReducer = (state = initialState, action) => {

    switch (action.type) {

        case ActionType.POST_LIST_FETCHING:
            return {
                ...state,
                postListFetching: true
            };

        case ActionType.POST_LIST_SUCCESS:
            return {
                ...state,
                postListFetching: false,
                postList: action.payload,
                postListLastError: undefined
            };

        case ActionType.POST_LIST_FAILURE:
            return {
                ...state,
                postListFetching: false,
                postListLastError: action.payload
            };
            
            case ActionType.POST_GET_FETCHING:
            return {
                ...state,
                postGetFetching: true
            };

        case ActionType.POST_GET_SUCCESS:
            return {
                ...state,
                postGetFetching: false,
                postGet: action.payload,
                postGetLastError: undefined
            };

        case ActionType.POST_GET_FAILURE:
            return {
                ...state,
                postGetFetching: false,
                postGetLastError: action.payload
            };

        case ActionType.POST_ADD_FETCHING:
            return {
                ...state,
                postAddFetching: true
            };

        case ActionType.POST_ADD_SUCCESS:
            return {
                ...state,
                postAddFetching: false,
                postAdd: action.payload,
                postAddLastError: undefined
            };

        case ActionType.POST_ADD_FAILURE:
            return {
                ...state,
                postAddFetching: false,
                postAddLastError: action.payload
            };

        case ActionType.COMMENT_ADD_REQUEST:
            return {
                ...state,
                commentAddFetching: true
            };

        case ActionType.COMMENT_ADD_SUCCESS:
            return {
                ...state,
                commentAddFetching: false,
                commentAdd: action.payload,
                commentAddLastError: undefined
            };

        case ActionType.COMMENT_ADD_FAILURE:
            return {
                ...state,
                commentAddFetching: false,
                commentAddLastError: action.payload
            };

        case ActionType.COMMENT_LIST_GET_BY_ID_SUCCESS:
            return {
                ...state,
                commentAddFetching: false,
                commentList: action.payload,
                commentAddLastError: undefined
            };
            
        default:
            return state;
    }
};