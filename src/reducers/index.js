import {applyMiddleware, combineReducers, createStore} from "redux";
import {connectionReducer} from "./connection-reducer";
import {authReducer} from "./auth-reducer";
import {postReducer} from "./post-reducer";
import {connectionMiddleware} from "./middleware/connection-middleware";
import {authMiddleware} from "./middleware/auth-middleware";
import {postMiddleware} from "./middleware/post-middleware";
import {pushNotificationReducer} from "./push-notification-reducer";

// Первоначальная настройка хранилища состояний Redux
export const store = createStore(
    combineReducers({
        // Перечисляем все reducer'ы: у каждого будет свое поле в объекте состояния Redux
        connection: connectionReducer,
        auth: authReducer,
        posts: postReducer,
        pushNotification: pushNotificationReducer
    }),
    applyMiddleware(
        // Перечисляем все middleware'ы
        connectionMiddleware,
        authMiddleware,
        postMiddleware
    )
);