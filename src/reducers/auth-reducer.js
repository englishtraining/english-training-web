import {ActionType} from "./actions/action-type";
import {authCookieName, removeCookie, setCookie} from "../service/CookieService";

const initialState = {
    user: undefined,
    lastError: undefined
};

export const authReducer = (state = initialState, action) => {

    switch (action.type) {

        case ActionType.AUTH_TOKEN_SUCCESS:

            console.log('User logged in by token:');
            console.log(action.payload);

            setCookie(authCookieName, action.payload.cookie);

            return {
                ...state,
                user: action.payload,
                lastError: undefined
            };

        case ActionType.AUTH_TOKEN_FAILURE:

            console.log('Token authentication error');
            console.log(action.payload);

            return {
                ...state,
                lastError: action.payload
            };

        case ActionType.AUTH_COOKIE_SUCCESS:

            console.log('User logged in by cookie:');
            console.log(action.payload);

            return {
                ...state,
                user: action.payload,
                lastError: undefined
            };

        case ActionType.AUTH_COOKIE_FAILURE:

            console.log('Token authentication error');
            console.log(action.payload);

            return {
                ...state,
                lastError: action.payload
            };

        case ActionType.AUTH_SIGN_OUT:

            console.log('User signed out:');
            console.log(state.user);

            removeCookie(authCookieName);

            return {
                ...state,
                user: undefined,
                lastError: undefined
            };

        default:
            return state;
    }
};