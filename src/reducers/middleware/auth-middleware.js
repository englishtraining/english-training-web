import {connectionSendAction} from "../actions/connection-actions";
import {ActionType} from "../actions/action-type";
import {pushErrorMessageAction, pushSuccessMessageAction} from "../actions/push-notification-actions";
import {formatUserName} from "../../service/UtilService";

export const authMiddleware = store => next => action => {

    switch (action.type) {

        case ActionType.AUTH_TOKEN_REQUEST:
        case ActionType.AUTH_COOKIE_REQUEST: {
            store.dispatch(connectionSendAction(action.payload));
            break;
        }

        case ActionType.AUTH_TOKEN_SUCCESS:
        case ActionType.AUTH_COOKIE_SUCCESS: {
            let message = `Вы вошли как ${formatUserName(action.payload)}.`;
            store.dispatch(pushSuccessMessageAction(message));
            return next(action);
        }

        case ActionType.AUTH_TOKEN_FAILURE:
        case ActionType.AUTH_COOKIE_FAILURE: {
            store.dispatch(pushErrorMessageAction('Произошла ошибка, или сервер закрыл соединение.'));
            return next(action);
        }

        case ActionType.AUTH_SIGN_OUT: {
            store.dispatch(pushSuccessMessageAction('Вы вышли как последний негодяй.'));
            return next(action);
        }

        default:
            return next(action);
    }
};