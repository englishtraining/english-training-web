import {ActionType} from "../actions/action-type";
import {connectionSendAction} from "../actions/connection-actions";
import {
    commentAddFetchingAction,
    commentListGetByIdFetchingAction,
    postAddFetchingAction,
    postGetFetchingAction,
    postListFetchingAction
} from "../actions/post-actions";
import {pushErrorMessageAction, pushSuccessMessageAction} from "../actions/push-notification-actions";

export const postMiddleware = store => next => {

    let message: '';

    return action => {

        switch (action.type) {

            case ActionType.POST_LIST_REQUEST:
                store.dispatch(postListFetchingAction());
                store.dispatch(connectionSendAction(action.payload));
                break;

            case ActionType.POST_GET_REQUEST:
                store.dispatch(postGetFetchingAction());
                store.dispatch(connectionSendAction(action.payload));
                break;

            case ActionType.POST_ADD_REQUEST:
                store.dispatch(postAddFetchingAction());
                store.dispatch(connectionSendAction(action.payload));
                break;

            case ActionType.POST_ADD_SUCCESS:
                store.dispatch(pushSuccessMessageAction('Статья успешно добавлена.'));
                return next(action);

            case ActionType.POST_ADD_FAILURE:
                let popupMsg = action.payload === 'USER_NOT_AUTHORIZED' ?
                    'Только авторизованные пользователи могут добавлять статьи!' :
                    'Произошла ошибка, или сервер закрыл соединение.';

                store.dispatch(pushErrorMessageAction(popupMsg));
                return next(action);

            case ActionType.COMMENT_ADD_REQUEST:
                store.dispatch(commentAddFetchingAction());
                store.dispatch(connectionSendAction(action.payload));
                break;

            case ActionType.COMMENT_ADD_SUCCESS:
                store.dispatch(pushSuccessMessageAction('Комментарий успешно добавлен.'));
                return next(action);

            case ActionType.COMMENT_ADD_FAILURE:
                message = action.payload === 'USER_NOT_AUTHORIZED' ?
                    'Только авторизованные пользователи могут добавлять комментарии!' :
                    'Произошла ошибка, или сервер закрыл соединение.';
                store.dispatch(pushErrorMessageAction(message));
                return next(action);

            case ActionType.COMMENT_LIST_GET_BY_ID_REQUEST:
                store.dispatch(commentListGetByIdFetchingAction());
                store.dispatch(connectionSendAction(action.payload));
                break;

            case ActionType.COMMENT_LIST_GET_BY_ID_FAILURE:
                message = action.payload;
                store.dispatch(pushErrorMessageAction(message));
                return next(action);

            default:
                return next(action);
        }
    };
};