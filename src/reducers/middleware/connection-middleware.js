import {ActionType} from "../actions/action-type";
import {
    connectionClosedAction,
    connectionErrorAction,
    connectionFetchingAction,
    connectionOpenAction,
    connectionOpenedAction,
    connectionReceiveAction,
    connectionSendAction
} from "../actions/connection-actions";
import {popupNotSuccess} from "../actions/action-popup";
import {RequestType} from "../../service/request-type";
import { ResponseType } from "../../service/responseType";
import {
    authCookieFailureAction,
    authCookieSuccessAction,
    authTokenFailureAction,
    authTokenSuccessAction
} from "../actions/auth-actions";
import {
    postAddFailureAction,
    postAddSuccessAction,
    postListFailureAction,
    postListSuccessAction,
    postGetSuccessAction,
    postGetFailureAction,
    commentAddSuccessAction,
    commentAddFailureAction,
    commentListGetByIdSuccessAction,
    commentListGetByIdFailureAction,
    commentListGetResponseAction
} from "../actions/post-actions";

// Счетчик сообщений (используется как id отправляемого сообщения)
let messageId = 1;

function nextMessageId() {
    return messageId++;
}

// Строим адрес сокета на основе текущего адреса
const host = window.location.host;
const protocol = window.location.protocol === 'http:' ? 'ws:' : 'wss:';
const socketName = 'websocket';

// const socketUrl = `${protocol}//${host}/${socketName}`;
// Для тестирования
const socketUrl = `wss://english-training48.xyz/${socketName}`;
// const socketUrl = `ws://localhost:8081`;

// Сокет
let socket;

// Типы уже отправленных запросов по messageId
let sentRequestTypes = {};

// Сообщения для отправки после успешной установки соединения ("пост-коннект-очередь" сообщений)
let connectionOpenedMessages = [];

// Получение действия-ответа по типу действия-запроса
function getResponseAction(requestType, data) {

    if (RequestType.ULOGIN_AUTH === requestType) {
        if (data.type === "error") {
            return authTokenFailureAction(data.error);
        }
        return authTokenSuccessAction(data.user);
    }

    if (RequestType.COOKIE_LOGIN_AUTH === requestType) {
        if (data.type === "error") {
            return authCookieFailureAction(data.error);
        }
        return authCookieSuccessAction(data.user);
    }

    if (RequestType.POST_LIST === requestType) {
        if (data.type === "error") {
            return postListFailureAction(data.error);
        }
        return postListSuccessAction(data.posts);
    }

    if (RequestType.POST_GET === requestType) {
        if (data.type === "error") {
            return postGetFailureAction(data.error);
        }

        return postGetSuccessAction(data.post);
    }

    if (RequestType.POST_ADD === requestType) {
        if (data.type === "error") {
            return postAddFailureAction(data.error);
        }

        return postAddSuccessAction(data.post);
    }
    
    if (RequestType.COMMENT_ADD === requestType) {
        if (data.type === "error") {
            return commentAddFailureAction(data.error);
        }

        return commentAddSuccessAction(data.comments);
    }
    
    if (RequestType.COMMENT_LIST_GET_BY_ID === requestType) {
        if (data.type === "error") {
            return commentListGetByIdFailureAction(data.error);
        }

        return commentListGetByIdSuccessAction(data.comments);
    }

    return null;
}

export const connectionMiddleware = store => next => action => {

    switch (action.type) {

        case ActionType.CONNECTION_OPEN:

            // Выполняем действие установки флага "соединение устанавливается"
            store.dispatch(connectionFetchingAction());

            console.log(`Connecting to ${socketUrl} ...`);

            socket = new WebSocket(socketUrl);

            socket.onopen = () => {

                // При успешной установке соединения:
                // - выполняем действие успешной установки соединения;
                // - отсылаем все накопившиеся в "пост-коннект-очереди" сообщения, а затем очищаем ее.
                store.dispatch(connectionOpenedAction());

                connectionOpenedMessages.forEach((message) => {
                    store.dispatch(connectionSendAction(message));
                });

                connectionOpenedMessages = [];
            };

            socket.onclose = (event) => {

                store.dispatch(connectionClosedAction(event));
                // Очищаем "пост-коннект-очередь" сообщений - при закрытии соединения они теряют свой смысл
                connectionOpenedMessages = [];
            };

            socket.onerror = (error) => {

                store.dispatch(connectionErrorAction(error));

                let data = {type: 'error', error: error};

                // Отправляем ошибку по всем уже отправленным сообщениям
                // Очищаем список отправленных сообщений
                for (let requestType in sentRequestTypes) {
                    if (sentRequestTypes.hasOwnProperty(requestType)) {
                        let responseAction = getResponseAction(requestType, data);
                        store.dispatch(responseAction);
                    }
                }
                sentRequestTypes = {};

                // Отправляем ошибку по всем сообщениям в "пост-коннект-очереди"
                // Очищаем "пост-коннект-очередь" сообщений
                connectionOpenedMessages.forEach((message) => {
                    let responseAction = getResponseAction(message.type, data);
                    store.dispatch(responseAction);
                });
                connectionOpenedMessages = [];
            };

            socket.onmessage = (event) => {
                store.dispatch(connectionReceiveAction(event));
            };

            break;

        case ActionType.CONNECTION_SEND:

            let connectionActive = store.getState().connection.active;

            // Если соединение активно, то отправляем сообщение
            if (connectionActive) {

                let messageId = nextMessageId();
                let message = JSON.stringify({...action.payload, messageId: messageId});

                sentRequestTypes[messageId] = action.payload.type;

                socket.send(message);

                console.log('Sending message...');
                console.log(message);
            }
            // Иначе:
            // - помещаем сообщение в "пост-коннект-очередь" сообщений;
            // - если нет активного запроса на установку соединения, то отправляем его.
            else {

                connectionOpenedMessages.push(action.payload);

                let connectionFetching = store.getState().connection.fetching;

                if (!connectionFetching) {
                    store.dispatch(connectionOpenAction());
                }
            }

            break;

        case ActionType.CONNECTION_RECEIVE:

            let data = JSON.parse(action.payload.data);

            let responseAction = getResponseAction(sentRequestTypes[data.messageId], data);

            store.dispatch(responseAction);

            delete sentRequestTypes[data.messageId];

            console.log('Message received:');
            console.log(data);

            break;

        default:
            return next(action);
    }
};