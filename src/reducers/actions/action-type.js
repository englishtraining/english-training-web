// Типы действий
export const ActionType = {

    // Соединение
    CONNECTION_OPEN: 'CONNECTION_OPEN',                             // Установка соединения
    CONNECTION_FETCHING: 'CONNECTION_FETCHING',                     // Установка флага "соединение устанавливается"
    CONNECTION_OPENED: 'CONNECTION_OPENED',                         // Соединение установлено
    CONNECTION_CLOSED: 'CONNECTION_CLOSED',                         // Соединение закрыто
    CONNECTION_ERROR: 'CONNECTION_ERROR',                           // Ошибка соединения
    CONNECTION_SEND: 'CONNECTION_SEND',                             // Отправка сообщения
    CONNECTION_RECEIVE: 'CONNECTION_RECEIVE',                       // Получение сообщения

    // Авторизация по токену
    AUTH_TOKEN_REQUEST: 'AUTH_TOKEN_REQUEST',                       // Запрос на авторизацию по токену
    AUTH_TOKEN_SUCCESS: 'AUTH_TOKEN_SUCCESS',                       // Авторизация по токену - успех
    AUTH_TOKEN_FAILURE: 'AUTH_TOKEN_FAILURE',                       // Авторизация по токену - ошибка

    // Авторизация по куке
    AUTH_COOKIE_REQUEST: 'AUTH_COOKIE_REQUEST',                     // Запрос на авторизацию по куке
    AUTH_COOKIE_SUCCESS: 'AUTH_COOKIE_SUCCESS',                     // Авторизация по куке - успех
    AUTH_COOKIE_FAILURE: 'AUTH_COOKIE_FAILURE',                     // Авторизация по куке - ошибка
    AUTH_SIGN_OUT: 'AUTH_SIGN_OUT',                                 // Логаут

    // Список статей
    POST_LIST_REQUEST: 'POST_LIST_REQUEST',                   // Запрос списка статей
    POST_LIST_FETCHING: 'POST_LIST_FETCHING',                 // Установка флага "список статей загружается"
    POST_LIST_SUCCESS: 'POST_LIST_SUCCESS',                   // Список статей - успех
    POST_LIST_FAILURE: 'POST_LIST_FAILURE',                   // Список статей - ошибка

    // Добавление статьи
    POST_ADD_REQUEST: 'POST_ADD_REQUEST',                     // Запрос добавления статьи
    POST_ADD_FETCHING: 'POST_ADD_FETCHING',                   // Установка флага "статья добавляется"
    POST_ADD_SUCCESS: 'POST_ADD_SUCCESS',                     // Добавление статьи - успех
    POST_ADD_FAILURE: 'POST_ADD_FAILURE',                     // Добавление статьи - ошибка

    // Получение статьи
    POST_GET_REQUEST: 'POST_GET_REQUEST',                     // Запрос статьи
    POST_GET_FETCHING: 'POST_GET_FETCHING',                   // Установка флага "статья загружается"
    POST_GET_SUCCESS: 'POST_GET_SUCCESS',                     // Получение статьи - успех
    POST_GET_FAILURE: 'POST_GET_FAILURE',                     // Получение статьи - ошибка

    // Push-уведомления
    PUSH_NOTIFICATION_ADD: 'PUSH_NOTIFICATION_ADD',
    PUSH_NOTIFICATION_REMOVE: 'PUSH_NOTIFICATION_REMOVE',

    // Всплывающее сообщение
    POPUP_SUCCESS: 'POPUP_SUCCESS',
    POPUP_FAILURE: 'POPUP_FAILURE',
    POPUP_HIDE: 'POPUP_HIDE',

    //Добавление комментария
    COMMENT_ADD_REQUEST: 'COMMENT_ADD_REQUEST',
    COMMENT_ADD_FETCHING: 'COMMENT_ADD_FETCHING',
    COMMENT_ADD_SUCCESS: 'COMMENT_ADD_SUCCESS',
    COMMENT_ADD_FAILURE: 'COMMENT_ADD_FAILURE',

    //Получение комментария
    COMMENT_LIST_GET_BY_ID_REQUEST: 'COMMENT_LIST_GET_BY_ID_REQUEST',
    COMMENT_LIST_GET_BY_ID_FETCHING: 'COMMENT_LIST_GET_BY_ID_FETCHING',
    COMMENT_LIST_GET_BY_ID_SUCCESS: 'COMMENT_LIST_GET_BY_ID_SUCCESS',
    COMMENT_LIST_GET_BY_ID_FAILURE: 'COMMENT_LIST_GET_BY_ID_FAILURE'
};