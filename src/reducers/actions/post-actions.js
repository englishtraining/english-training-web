import {ActionType} from "./action-type";
import {RequestType} from "../../service/request-type";

// Действия для работы со статьями

export const postListRequestAction = () => ({
    type: ActionType.POST_LIST_REQUEST,
    payload: {
        type: RequestType.POST_LIST
    }
});

export const postListFetchingAction = () => ({
    type: ActionType.POST_LIST_FETCHING
});

export const postListSuccessAction = (postList) => ({
    type: ActionType.POST_LIST_SUCCESS,
    payload: postList
});

export const postListFailureAction = (error) => ({
    type: ActionType.POST_LIST_FAILURE,
    payload: error
});

export const postAddRequestAction = (post) => ({
    type: ActionType.POST_ADD_REQUEST,
    payload: {
        type: RequestType.POST_ADD,
        ...post
    }
});

export const postAddFetchingAction = () => ({
    type: ActionType.POST_ADD_FETCHING
});

export const postAddSuccessAction = (post) => ({
    type: ActionType.POST_ADD_SUCCESS,
    payload: post
});

export const postAddFailureAction = (error) => ({
    type: ActionType.POST_ADD_FAILURE,
    payload: error
});

export const commentAddRequestAction = (comment) => ({
    type: ActionType.COMMENT_ADD_REQUEST,
    payload: {
        type: RequestType.COMMENT_ADD,
        ...comment
    }
});

export const commentAddFetchingAction = () => ({
    type: ActionType.COMMENT_ADD_FETCHING
});

export const commentAddSuccessAction = (commentList) => ({
    type: ActionType.COMMENT_ADD_SUCCESS,
    payload: commentList
});

export const commentAddFailureAction = (error) => ({
    type: ActionType.COMMENT_ADD_FAILURE,
    payload: error
});

export const postGetRequestAction = (postId) => ({
    type: ActionType.POST_GET_REQUEST,
    payload: {
        type: RequestType.POST_GET,
        postId: postId
    }
});

export const postGetFetchingAction = () => ({
    type: ActionType.POST_GET_FETCHING
});

export const postGetSuccessAction = (post) => ({
    type: ActionType.POST_GET_SUCCESS,
    payload: post
});

export const postGetFailureAction = (error) => ({
    type: ActionType.POST_GET_FAILURE,
    payload: error
});

export const commentListGetByIdRequestAction = (commentId) => ({
    type: ActionType.COMMENT_LIST_GET_BY_ID_REQUEST,
    payload: {
        type: RequestType.COMMENT_LIST_GET_BY_ID,
        commentWallId: commentId
    }
});

export const commentListGetByIdFetchingAction = () => ({
    type: ActionType.COMMENT_LIST_GET_BY_ID_FETCHING
});

export const commentListGetByIdSuccessAction = (commentList) => ({
    type: ActionType.COMMENT_LIST_GET_BY_ID_SUCCESS,
    payload: commentList
});

export const commentListGetByIdFailureAction = (error) => ({
    type: ActionType.COMMENT_LIST_GET_BY_ID_FAILURE,
    payload: error
});