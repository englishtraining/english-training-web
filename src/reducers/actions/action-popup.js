import { ActionType } from "./action-type";

// Действия для уведомлений

export const popupSuccess = message => ({
    type: ActionType.POPUP_SUCCESS,
    payload: message
});

export const popupFailure = message => ({
    type: ActionType.POPUP_FAILURE,
    payload: message
});

export const popupHide = () => ({
    type: ActionType.POPUP_HIDE,
});
