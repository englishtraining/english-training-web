import {ActionType} from "./action-type";

const defaultDuration = 5000;

function createMessage(color, icon, duration, message) {
    return {
        color: color,
        icon: icon,
        duration: duration,
        message: message
    };
}

export const pushSuccessMessageAction = (message, duration = defaultDuration) => ({
    type: ActionType.PUSH_NOTIFICATION_ADD,
    payload: createMessage('green', 'checkmark', duration, message)
});

export const pushErrorMessageAction = (message, duration = defaultDuration) => ({
    type: ActionType.PUSH_NOTIFICATION_ADD,
    payload: createMessage('red', 'ban', duration, message)
});

export const pushInfoMessageAction = (message, duration = defaultDuration) => ({
    type: ActionType.PUSH_NOTIFICATION_ADD,
    payload: createMessage('blue', 'info', duration, message)
});

export const pushWarningMessageAction = (message, duration = defaultDuration) => ({
    type: ActionType.PUSH_NOTIFICATION_ADD,
    payload: createMessage('brown', 'warning', duration, message)
});

export const removeMessageAction = (messageId) => ({
    type: ActionType.PUSH_NOTIFICATION_REMOVE,
    payload: messageId
});