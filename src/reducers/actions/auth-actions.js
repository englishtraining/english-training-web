// Действия авторизации

import {ActionType} from "./action-type";
import {RequestType} from "../../service/request-type";

export const authTokenRequestAction = (token) => ({
    type: ActionType.AUTH_TOKEN_REQUEST,
    payload: {
        type: RequestType.ULOGIN_AUTH,
        token: token
    }
});

export const authTokenSuccessAction = (userInfo) => ({
    type: ActionType.AUTH_TOKEN_SUCCESS,
    payload: userInfo
});

export const authTokenFailureAction = (error) => ({
    type: ActionType.AUTH_TOKEN_FAILURE,
    payload: error
});

export const authCookieRequestAction = (cookie) => ({
    type: ActionType.AUTH_COOKIE_REQUEST,
    payload: {
        type: RequestType.COOKIE_LOGIN_AUTH,
        cookie: cookie
    }
});

export const authCookieSuccessAction = (userInfo) => ({
    type: ActionType.AUTH_COOKIE_SUCCESS,
    payload: userInfo
});

export const authCookieFailureAction = (error) => ({
    type: ActionType.AUTH_COOKIE_FAILURE,
    payload: error
});

export const authSignOutAction = () => ({
    type: ActionType.AUTH_SIGN_OUT
});