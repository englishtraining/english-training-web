import {ActionType} from "./action-type";

export const connectionOpenAction = () => ({
    type: ActionType.CONNECTION_OPEN
});

export const connectionFetchingAction = () => ({
    type: ActionType.CONNECTION_FETCHING
});

export const connectionOpenedAction = () => ({
    type: ActionType.CONNECTION_OPENED
});

export const connectionClosedAction = (event) => ({
    type: ActionType.CONNECTION_CLOSED,
    payload: event
});

export const connectionErrorAction = (error) => ({
    type: ActionType.CONNECTION_ERROR,
    payload: error
});

export const connectionSendAction = (message) => ({
    type: ActionType.CONNECTION_SEND,
    payload: message
});

export const connectionReceiveAction = (event) => ({
    type: ActionType.CONNECTION_RECEIVE,
    payload: event
});