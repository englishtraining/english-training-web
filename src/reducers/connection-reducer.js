import {ActionType} from "./actions/action-type";

const initialState = {
    active: false,
    fetching: false,
    lastError: undefined
};

export const connectionReducer = (state = initialState, action) => {

    switch (action.type) {

        case ActionType.CONNECTION_FETCHING:
            return {
                ...state,
                fetching: true
            };

        case ActionType.CONNECTION_OPENED:
            return {
                ...state,
                active: true,
                fetching: false,
                lastError: undefined
            };

        case ActionType.CONNECTION_CLOSED:
            return {
                ...state,
                active: false,
                fetching: false,
                lastError: undefined
            };

        case ActionType.CONNECTION_ERROR:
            return {
                ...state,
                fetching: false,
                lastError: action.payload
            };

        default:
            return state;
    }
};