import {ActionType} from "./actions/action-type";

const initialState = {
    messages: []
};

let messageId = 1;

export const pushNotificationReducer = (state = initialState, action) => {

    switch (action.type) {

        case ActionType.PUSH_NOTIFICATION_ADD: {

            let message = {
                ...action.payload,
                messageId: messageId++
            };

            let newMessages = state.messages.slice();
            newMessages.push(message);

            return {
                messages: newMessages
            };
        }

        case ActionType.PUSH_NOTIFICATION_REMOVE: {

            let messageId = action.payload;

            let newMessages = state.messages.filter(message => message.messageId !== messageId);

            return {
                messages: newMessages
            };
        }

        default:
            return state;
    }
};